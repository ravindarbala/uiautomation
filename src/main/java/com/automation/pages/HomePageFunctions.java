package com.automation.pages;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePageFunctions extends UIInteractionSteps {
  private static final Logger LOGGER = LoggerFactory.getLogger(HomePageFunctions.class);

  @Step("Get Hero Title")
  public String getHeroTitle() {
    $(HomePageElements.HOME_PAGE).waitUntilPresent();
    return $(HomePageElements.CURRENT_TITLE_SECTION).findElement(By.cssSelector("h1[data-testid='hero-title']")).getText();
  }
  @Step("Click on Details Button")
    public void navigateToDetailsPage() {
    $(HomePageElements.HERO_DETAILS_BUTTON).click();
  }
  public String getHeroTitleFromDetailsPage(){
    LOGGER.info("Inside the getHeroTitleFromDetailsPage Method");
    navigateToDetailsPage();
    return $(HomePageElements.HERO_TITLE).waitUntilPresent().getText();
   }

}
