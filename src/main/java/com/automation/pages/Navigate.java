package com.automation.pages;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;


public class Navigate extends UIInteractionSteps {
  HomePageElements homePageElements;

  @Step("Open the  home page")
  public void theHomePage() {
    homePageElements.open();
  }

}
