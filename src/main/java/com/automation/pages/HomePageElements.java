package com.automation.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://www.skygo.co.nz/")
public class HomePageElements extends PageObject {


  public static By HOME_PAGE = By.cssSelector("a[data-testid='header-home']");

  public static By CURRENT_TITLE_SECTION =By.cssSelector("div[data-index='0']");

  public static By HERO_TITLE =By.cssSelector("h1[data-testid='hero-title']");

  public static By HERO_DETAILS_BUTTON =By.name("Hero banner details button");




}
