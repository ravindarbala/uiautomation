package com.automation.steps;


import cucumber.api.java.en.Then;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePageStepDef extends AbstractStepDef {
  private static final Logger LOGGER = LoggerFactory.getLogger(HomePageStepDef.class);

  @Then("the user compares the hero tile from home page with details page")
  public void theUserComparesTheHeroTileFromHomePageWithDetailsPage() {
    String heroTitleFromHompage = homePageFunctions.getHeroTitle();
    LOGGER.info(heroTitleFromHompage);
    String heroTitleFromDetailsPage = homePageFunctions.getHeroTitleFromDetailsPage();
    LOGGER.info(heroTitleFromDetailsPage);
    Assert.assertEquals(heroTitleFromHompage,heroTitleFromDetailsPage);

  }
}
