package com.automation.steps;


import com.automation.pages.HomePageFunctions;
import com.automation.pages.Navigate;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Steps;

public class AbstractStepDef extends UIInteractionSteps {

  @Steps
  Navigate navigateTo;
  @Steps
  HomePageFunctions homePageFunctions;
}
