package com.automation.steps;

import cucumber.api.java.en.Given;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonStepDef extends AbstractStepDef {

  private static final Logger LOGGER = LoggerFactory.getLogger(CommonStepDef.class);

  @Given("the user opens the website")
  public void theUserOpensTheWebsite() {
    LOGGER.info("Inside the theUserOpensTheWebsite method ");
    navigateTo.theHomePage();
  }


}
