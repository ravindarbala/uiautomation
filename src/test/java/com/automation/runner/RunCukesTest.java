package com.automation.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		glue = { "com.automation.steps" },
		plugin = { "html:build/cucumber-html-report",
					"pretty:build/cucumber-pretty.txt",
					"json:build/cucumber.json" }, 
		features = { "src/test/resources/features/" }
		)
public class RunCukesTest {
}
