# UI Automation Tests
This project built using serenity BDD with Page object design pattern
for logging we have used slf4j logger

[serenity]: https://serenity-bdd.github.io/theserenitybook/latest/cucumber.html

## Prerequisites
* Git
* Java 8 or Java 11 (Project not compatible with Java 16)
* IDE - This project was setup using [IntelliJ](https://www.jetbrains.com/idea/download/)
    * Additional IntelliJ plugins: Cucumber for Java, Gherkin,Maven

## Quickstart
* Clone project to local directory
  ```
  git clone https://bitbucket.org/ravindarbala/uiautomation.git
 
  download latest chromedriver from https://chromedriver.chromium.org/downloads and place in src/test/resources/drivers/windows
  
  To run all the feature files right click and run as RunCukesTest.java from src\test\java\com\automation\runner\RunCukesTest.java
 
  To run single feature right click on feature and run feature ex :\src\test\resources\features\home.feature
  
  ```
  
* Run through terminal and generate the serenity report using below commands
  ## To run all BDD scenarios use
   mvn verify 
  ## To generate the report use
    mvn serenity:aggregate
 ## To view the report open the index.html file from the below path
   UiAutomation\target\site\serenity\index.html
  
